package backend.controller.superadmin;

import backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/superadmin/project")
public class ProjectConrtoller {
    private final ProjectService projectService;

    @Autowired
    public ProjectConrtoller(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PutMapping("/accept/{projectId}")
    @Secured({"ROLE_SUPERUSER"})
    public Map acceptProject(@PathVariable() Long projectId) {
        return projectService.acceptProject(projectId);
    }

    @PutMapping("/decline/{projectId}")
    @Secured({"ROLE_SUPERUSER"})
    public Map declineProject(@PathVariable() Long projectId) {
        return projectService.declineProject(projectId);
    }

    @GetMapping("/")
    @Secured({"ROLE_SUPERUSER"})
    public Map getProject() {
        return projectService.getAll();
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_SUPERUSER"})
    public Map getDetailProject(@PathVariable() Long id) {
        return projectService.getDetailRequest(id);
    }


}
