package backend.controller.superadmin;

import backend.service.HistoryProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/superadmin/accessmoney")
public class HistoryController {

    private final HistoryProjectService historyProjectService;

    @Autowired
    public HistoryController(HistoryProjectService historyProjectService) {
        this.historyProjectService = historyProjectService;
    }

    @PutMapping("/accept/{projectId}")
    @Secured({"ROLE_SUPERUSER"})
    public Map acceptProjectHistory(@PathVariable() Long projectId) {
        return historyProjectService.acceptProject(projectId);
    }

    @PutMapping("/decline/{projectId}")
    @Secured({"ROLE_SUPERUSER"})
    public Map declineProjectHistory(@PathVariable() Long projectId) {
        return historyProjectService.declineProject(projectId);
    }

    @GetMapping("/")
    @Secured({"ROLE_SUPERUSER"})
    public Map getProjectHistory() {
        return historyProjectService.getAll();
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_SUPERUSER"})
    public Map getDetailProjectHistory(@PathVariable() Long id) {
        return historyProjectService.getOne(id);
    }

}
