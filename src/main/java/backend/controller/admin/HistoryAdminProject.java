package backend.controller.admin;

import backend.entity.Project;
import backend.repository.UserRepository;
import backend.service.HistoryProjectService;
import backend.service.ProjectService;
import backend.service.oauth.Oauth2UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping(path = "/admin/history")
public class HistoryAdminProject {

    @Autowired
    private HistoryProjectService historyProjectService;

    @Autowired
    private Oauth2UserDetailsService oauth2UserDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectService projectService;

    @GetMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getAllHistory(Principal principal) {
        UserDetails user = oauth2UserDetailsService.loadUserByUsername(principal.getName());
        Project project = new Project();
        project.setUser(userRepository.findOneByUsername(user.getUsername()));
        return new ResponseEntity<>(projectService.getByAdminId(project), HttpStatus.OK);
    }

    @GetMapping("/{projectId}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getHistory(@PathVariable() Long projectId) {
        return new ResponseEntity<>(historyProjectService.viewRequest(projectId), HttpStatus.OK);
    }

    @PutMapping("/request/{projectId}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> requestAccessMoney(@PathVariable() Long projectId) {
        return new ResponseEntity<>(historyProjectService.request(projectId), HttpStatus.OK);
    }

}
