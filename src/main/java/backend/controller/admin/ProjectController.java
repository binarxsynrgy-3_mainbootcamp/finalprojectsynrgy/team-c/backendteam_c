package backend.controller.admin;

import backend.entity.Project;
import backend.repository.UserRepository;
import backend.service.ProjectService;
import backend.service.oauth.Oauth2UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping(path = "/admin/project")
public class ProjectController {

    private final ProjectService projectService;

    private final UserRepository userRepository;

    private final Oauth2UserDetailsService oauth2UserDetailsService;

    @Autowired
    public ProjectController(ProjectService projectService,
                             UserRepository userRepository,
                             Oauth2UserDetailsService oauth2UserDetailsService) {
        this.projectService = projectService;
        this.userRepository = userRepository;
        this.oauth2UserDetailsService = oauth2UserDetailsService;
    }

    @GetMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getProjectAdmin(Principal principal) {
        UserDetails user = oauth2UserDetailsService.loadUserByUsername(principal.getName());
        Project project = new Project();
        project.setUser(userRepository.findOneByUsername(user.getUsername()));
        return new ResponseEntity<>(projectService.getByAdminId(project), HttpStatus.OK);
    }

    @PostMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> postProject(@RequestBody Project project) {
        return new ResponseEntity<>(projectService.insert(project), HttpStatus.OK);
    }

    @PutMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> updateProject(@RequestBody Project project) {
        return new ResponseEntity<>(projectService.update(project), HttpStatus.OK);
    }

    @GetMapping("{projectId}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getProjectAdminById(@PathVariable() Long projectId) {
        return new ResponseEntity<>(projectService.getOne(projectId), HttpStatus.OK);
    }

}
