package backend.controller.main.forgetpassword;

import org.springframework.stereotype.Component;

@Component("emailTemplate")
public class EmailTemplate {

    public String getResetPassword() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style>\n" +
                "\t.email-container {\n" +
                "\t\tpadding-top: 10px;\n" +
                "\t}\n" +
                "\tp {\n" +
                "\t\ttext-align: left;\n" +
                "\t}\n" +
                "\n" +
                "\ta.btn {\n" +
                "\t\tdisplay: block;\n" +
                "\t\tmargin: 30px auto;\n" +
                "\t\tbackground-color: #01c853;\n" +
                "\t\tpadding: 10px 20px;\n" +
                "\t\tcolor: #fff;\n" +
                "\t\ttext-decoration: none;\n" +
                "\t\twidth: 30%;\n" +
                "\t\ttext-align: center;\n" +
                "\t\tborder: 1px solid #01c853;\n" +
                "\t\ttext-transform: uppercase;\n" +
                "\t}\n" +
                "\ta.btn:hover,\n" +
                "\ta.btn:focus {\n" +
                "\t\tcolor: #01c853;\n" +
                "\t\tbackground-color: #fff;\n" +
                "\t\tborder: 1px solid #01c853;\n" +
                "\t}\n" +
                "\t.user-name {\n" +
                "\t\ttext-transform: uppercase;\n" +
                "\t}\n" +
                "\t.manual-link,\n" +
                "\t.manual-link:hover,\n" +
                "\t.manual-link:focus {\n" +
                "\t\tdisplay: block;\n" +
                "\t\tcolor: #396fad;\n" +
                "\t\tfont-weight: bold;\n" +
                "\t\tmargin-top: -15px;\n" +
                "\t}\n" +
                "\t.mt--15 {\n" +
                "\t\tmargin-top: -15px;\n" +
                "\t}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div class=\"email-container\">\n" +
                "\t\t<p>Hai Pelanggan Yth,</p>\n" +
                "\t\t<p>Rahasia! Jangan berikan kode RAHASIA ini ke siapa pun termasuk<br>\n" +
                "\t\tstaf toBantoo. Gunakan kode <b>{{PASS_TOKEN}}</b> untuk mereset password kamu.</p>\n" +
                "\t\t<p>Jika kamu tidak melakukan aktivitas ini silakan hubungi toBantoo<br>\n" +
                "\t\tCustomer Centre kami di 082200030010.</p>\n" +
                "\t\t<p>Nikmati keamanan dan kenyamanan dalam setiap transaksi<br>\n" +
                "\t\tKebaikan kamu dengan toBantoo.</p>\n" +
                "\t\t<br>\n" +
                "\t\t<p>Salam hangat,</p>\n" +
                "\t\t<p>toBantoo by Team C</p>\n" +
                "\t</div>\n" +
                "</body>\n" +
                "</html>";
    }

    public String getPaymentCampaign() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style>\n" +
                "\t.email-container {\n" +
                "\t\tpadding-top: 10px;\n" +
                "\t}\n" +
                "\tp {\n" +
                "\t\ttext-align: left;\n" +
                "\t}\n" +
                "\n" +
                "\ta.btn {\n" +
                "\t\tdisplay: block;\n" +
                "\t\tmargin: 30px auto;\n" +
                "\t\tbackground-color: #01c853;\n" +
                "\t\tpadding: 10px 20px;\n" +
                "\t\tcolor: #fff;\n" +
                "\t\ttext-decoration: none;\n" +
                "\t\twidth: 30%;\n" +
                "\t\ttext-align: center;\n" +
                "\t\tborder: 1px solid #01c853;\n" +
                "\t\ttext-transform: uppercase;\n" +
                "\t}\n" +
                "\ta.btn:hover,\n" +
                "\ta.btn:focus {\n" +
                "\t\tcolor: #01c853;\n" +
                "\t\tbackground-color: #fff;\n" +
                "\t\tborder: 1px solid #01c853;\n" +
                "\t}\n" +
                "\t.user-name {\n" +
                "\t\ttext-transform: uppercase;\n" +
                "\t}\n" +
                "\t.manual-link,\n" +
                "\t.manual-link:hover,\n" +
                "\t.manual-link:focus {\n" +
                "\t\tdisplay: block;\n" +
                "\t\tcolor: #396fad;\n" +
                "\t\tfont-weight: bold;\n" +
                "\t\tmargin-top: -15px;\n" +
                "\t}\n" +
                "\t.mt--15 {\n" +
                "\t\tmargin-top: -15px;\n" +
                "\t}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div class=\"email-container\">\n" +
                "\t\t<p>Hai Pelanggan Yth,</p>\n" +
                "\t\t<p>Rahasia! Jangan berikan kode RAHASIA ini ke siapa pun termasuk<br>\n" +
                "\t\tstaf toBantoo. Gunakan kode <b>{{PASS_TOKEN}}</b> untuk mengkonfirmasi pembayaran campaign.</p>\n" +
                "\t\t<p>Jika kamu tidak melakukan aktivitas ini silakan hubungi toBantoo<br>\n" +
                "\t\tCustomer Centre kami di 082200030010.</p>\n" +
                "\t\t<p>Nikmati keamanan dan kenyamanan dalam setiap transaksi<br>\n" +
                "\t\tKebaikan kamu dengan toBantoo.</p>\n" +
                "\t\t<br>\n" +
                "\t\t<p>Salam hangat,</p>\n" +
                "\t\t<p>toBantoo by Team C</p>\n" +
                "\t</div>\n" +
                "</body>\n" +
                "</html>";
    }

}
