package backend.controller.main;

import backend.config.UploadFileResponse;

import backend.entity.Profile;
import backend.model.UpdatePasswordModel;
import backend.repository.ProfileRepository;
import backend.service.FileStorageService;
import backend.service.ProfileService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;


@RestController
@RequestMapping(path = "/profile")
class ProfileControllers {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(
            ProfileControllers.class
    );

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ProfileRepository profileRepository;


    public ProfileService profileService;
    @Autowired
    ProfileControllers(FileStorageService fileStorageService, ProfileService profileService) {
        this.fileStorageService = fileStorageService;
        this.profileService = profileService;
    }

    @RequestMapping(
            value = "/photo/upload",
            method = RequestMethod.POST,
            consumes = {"multipart/form-data", "application/json"}
    )
    public UploadFileResponse uploadFile(@RequestParam() MultipartFile file,
                                         @RequestParam() Long id) throws IOException {
        try {
            fileStorageService.storeFile(file);
            Profile obj = profileRepository.getByUserId(id);
            obj.setPic_profile(file.getOriginalFilename());
            profileRepository.save(obj);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new UploadFileResponse(
                    file.getOriginalFilename(),
                    null,
                    file.getContentType(),
                    file.getSize(),
                    e.getMessage()
            );
        }

        String fileDownloadUri = ServletUriComponentsBuilder.
                fromCurrentContextPath().
                path("/showFile/").
                path(file.getOriginalFilename()).
                toUriString();

        return new UploadFileResponse(
                file.getOriginalFilename(),
                fileDownloadUri,
                file.getContentType(),
                file.getSize(),
                "false"
        );
    }

    @GetMapping("/photo/showfile/{fileName:.+}")
    public ResponseEntity<Resource> showFile(@PathVariable String fileName,
                                             HttpServletRequest request) {
        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(
                    fileStorageService.
                            loadFileAsResource(fileName).
                            getFile().
                            getAbsolutePath()
            );
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\""
                                + fileStorageService.loadFileAsResource(fileName).getFilename()
                                + "\""
                ).
                body(fileStorageService.loadFileAsResource(fileName));
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map viewProfile(@PathVariable() Long id) {
        return profileService.viewDetail(id);


    }

    @PutMapping("/editprofile")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map updateProfile(  @Valid @RequestBody  Profile profile) {
        return profileService.update(profile);
    }

    @PutMapping("/updatepassword")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Map> updatePassword(@RequestBody UpdatePasswordModel updatePasswordModel) {
        return new ResponseEntity<>(profileService.changePassword(updatePasswordModel), HttpStatus.OK);
    }

}




