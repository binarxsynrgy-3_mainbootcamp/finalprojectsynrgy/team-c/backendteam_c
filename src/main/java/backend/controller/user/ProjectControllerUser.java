package backend.controller.user;

import backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;

//@RestController
@RequestMapping(path = "/project")

public class ProjectControllerUser {
    private final ProjectService projectService;

    @Autowired
    public ProjectControllerUser(ProjectService projectService) {
        this.projectService = projectService;
    }

}
