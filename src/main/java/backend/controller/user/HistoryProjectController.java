package backend.controller.user;

import backend.entity.HistoryProject;
import backend.repository.UserRepository;
import backend.service.HistoryProjectService;
import backend.service.ProjectService;
import backend.service.oauth.Oauth2UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping(path = "/user")
public class HistoryProjectController {

    private final ProjectService projectService;

    private final HistoryProjectService historyProjectService;

    private final UserRepository userRepository;

    private final Oauth2UserDetailsService oauth2UserDetailsService;

    @Autowired
    public HistoryProjectController(ProjectService projectService,
                                    HistoryProjectService historyProjectService,
                                    UserRepository userRepository,
                                    Oauth2UserDetailsService oauth2UserDetailsService) {
        this.historyProjectService = historyProjectService;
        this.projectService = projectService;
        this.userRepository = userRepository;
        this.oauth2UserDetailsService = oauth2UserDetailsService;
    }

    @GetMapping("/donate")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getListApproved() {
        return new ResponseEntity<>(projectService.getListApproved(), HttpStatus.OK);
    }

    @GetMapping("/donate/{projectId}")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getProject(@PathVariable() Long projectId) {
        return new ResponseEntity<>(projectService.getOne(projectId), HttpStatus.OK);
    }

    @PostMapping("/donate/{projectId}")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> postPayment(@PathVariable() Long projectId,
                                           @RequestBody HistoryProject historyProject,
                                           Principal principal) {
        historyProject.setUser(
                userRepository.findOneByUsername(
                        oauth2UserDetailsService.loadUserByUsername(
                                principal.getName()
                        ).getUsername()
                )
        );
        return new ResponseEntity<>(historyProjectService.insert(historyProject, projectId), HttpStatus.OK);
    }

    @GetMapping("/donate/history")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> viewHistory(Principal principal) {
        return new ResponseEntity<>(
                historyProjectService.viewHistory(
                        userRepository.findOneByUsername(
                                oauth2UserDetailsService.loadUserByUsername(
                                        principal.getName()
                                ).getUsername()
                        ).getId()
                ), HttpStatus.OK
        );
    }

    @GetMapping("/search")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getListSearchByProjectName(@RequestParam() String projectName) {
        return new ResponseEntity<>(projectService.getProjectByProjectName(projectName), HttpStatus.OK);
    }

    @GetMapping("/filter")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getListSearchByCategory(@RequestParam() String category) {
        return new ResponseEntity<>(projectService.getProjectByCategory(category), HttpStatus.OK);
    }

    @GetMapping("/sorting/mendesak")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getListMendesak() {
        return new ResponseEntity<>(projectService.getProjectByDaysLeft(), HttpStatus.OK);
    }

    @GetMapping("/sorting/terbaru")
    @Secured({"ROLE_USER"})
    public ResponseEntity<Map> getListterbaru() {
        return new ResponseEntity<>(projectService.getProjectByTerbaru(), HttpStatus.OK);
    }

}
