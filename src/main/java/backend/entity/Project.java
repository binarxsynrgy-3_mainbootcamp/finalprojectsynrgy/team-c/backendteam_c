package backend.entity;

import lombok.*;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name = "project")
@Where(clause = "deleted_date is null")
public class Project extends AbstractDate implements Serializable, Comparable {

    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "project_sequence"
    )
    private Long id;

    private String projectName;

    private String category;

    @Transient
    private int duration;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate projectEnd;

    private String profilePeneliti;

    private String institution;

    private String projectSummary;

    @Column(nullable = true)
    private String projectImg;

    private String budget;

    private String budgetDetails;

    private String projectBackground;

    private String projectUrgency;

    private String projectGoal;

    private String researchMethods;

    @Column(nullable = true)
    private String projectDetails;

    @Column(nullable = true)
    private String coverLetter;

    @Column(nullable = true)
    private Boolean status;

    private Long donateAll = 0L;

    @Column(nullable = true)
    private Boolean statusDonate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public int compareTo(Object o) {
        return this.duration - ((Project) o).getDuration();
    }

}