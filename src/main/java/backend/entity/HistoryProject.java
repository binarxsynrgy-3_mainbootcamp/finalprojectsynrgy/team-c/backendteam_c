package backend.entity;

import lombok.Data;
import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "history_project")
@Where(clause = "deleted_date is null")
public class HistoryProject extends AbstractDate implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "totalDonate", length = 20, nullable = false)
    private Long totalDonate;

    private String paymentMethods;

    @Value("${some.key:false}")
    private boolean status;

    @Column(length = 100, nullable = true)
    private String otp;

    private Date otpExpiredDate;

    @Value("${some.key:false}")
    private boolean requestStatus;

    private String nomorRekening;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
