package backend.model;

import lombok.Data;

@Data
public class UpdatePasswordModel {

    private String email;

    private String oldPassword;

    private String newPassword1;

    private String newPassword2;

}
