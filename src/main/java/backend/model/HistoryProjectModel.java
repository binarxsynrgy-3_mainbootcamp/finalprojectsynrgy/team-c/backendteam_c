package backend.model;

import lombok.Data;

@Data
public class HistoryProjectModel {

    public Long id;

    private String totalDonate;

    private boolean status;

    public String otp;

}
