package backend.service;

import backend.entity.HistoryProject;
import backend.model.HistoryProjectModel;

import java.util.Map;

public interface HistoryProjectService {

    public Map insert(HistoryProject historyProject, long projectIs);

    // super admin
    Map getAll();

    Map getOne(Long id);

    Map acceptProject(Long id);

    Map declineProject(Long id);

    public Map verifDonate(HistoryProject historyProject);

    public Map viewHistory(Long userId);

    //mahasiswa
    public Map viewRequest(Long projectId);

    public Map request(Long projectId);
}
