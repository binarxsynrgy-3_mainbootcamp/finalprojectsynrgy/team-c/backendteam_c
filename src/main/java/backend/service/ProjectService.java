package backend.service;

import backend.entity.Project;

import java.util.Map;

public interface ProjectService {

    Map insert(Project project);

    Map getByAdminId(Project project);

    Map update(Project project);

    Map getOne(Long id);

    // super admin
    Map getAll();

    Map getDetailRequest(Long id);

    Map acceptProject(Long id);

    Map declineProject(Long id);

    Map getListApproved();

    Map getProjectByProjectName(String projectName);

    Map getProjectByCategory(String category);

    Map getProjectByDaysLeft();

    Map getProjectByTerbaru();
    //sorting filtering
    Map getListPalingSedikit();


}
