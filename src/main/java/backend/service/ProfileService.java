package backend.service;

import backend.config.UploadFileResponse;
import backend.entity.Profile;
import backend.model.UpdatePasswordModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface ProfileService {
    Map viewDetail(Long profileId);
    Map update(Profile profile);
    UploadFileResponse uploadProfile(MultipartFile file,
                                     Long id);
//    Map insert(Profile profile);

//  Service untuk ubah password di profile
    Map changePassword(UpdatePasswordModel updatePasswordModel);

}
