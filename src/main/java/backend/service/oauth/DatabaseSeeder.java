package backend.service.oauth;

import backend.entity.*;
import backend.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
@Service
public class DatabaseSeeder implements ApplicationRunner {

    private static final String TAG = "DatabaseSeeder {}";

    private final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    private final PasswordEncoder encoder;

    private final RoleRepository roleRepository;

    private final ClientRepository clientRepository;

    private final UserRepository userRepository;

    private final RolePathRepository rolePathRepository;

    private final ProjectRepository projectRepository;

    private final ProfileRepository profileRepository;

    private final String[] users = new String[]{
            "tobantoo.adm@gmail.com:ROLE_SUPERUSER ROLE_USER ROLE_ADMIN:toBantoo",
            "mahasiswa@gmail.com:ROLE_ADMIN:Mahasiswa",
            "donatur@gmail.com:ROLE_USER:Donatur"
    };

    private final String[] clients = new String[]{
            "my-client-apps:ROLE_READ ROLE_WRITE",
            "my-client-web:ROLE_READ ROLE_WRITE"
    };

    private final String[] roles = new String[]{
            "ROLE_SUPERUSER:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_ADMIN:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_USER:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_READ:oauth_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_WRITE:oauth_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS"
    };

    private final String[] project = new String[]{
            "The Possibility of Unicorns:Biologi:University of Saad Maan:30:Lorem20:10000000",
            "The Sam Sung:Fisika:University of Saad Maan:20:Lorem20:5000000"
    };

    @Autowired
    public DatabaseSeeder(PasswordEncoder encoder,
                          RoleRepository roleRepository,
                          ClientRepository clientRepository,
                          UserRepository userRepository,
                          RolePathRepository rolePathRepository,
                          ProjectRepository projectRepository,
                          ProfileRepository profileRepository) {
        this.encoder = encoder;
        this.roleRepository = roleRepository;
        this.clientRepository = clientRepository;
        this.userRepository = userRepository;
        this.rolePathRepository = rolePathRepository;
        this.projectRepository = projectRepository;
        this.profileRepository = profileRepository;
    }

    @Override
    @Transactional
    public void run(ApplicationArguments applicationArguments) {
        String defaultPassword = "password";
        String password = encoder.encode(defaultPassword);

        this.insertRoles();
        this.insertClients(password);
        this.insertUser(password);
        this.insertProject();
    }

    @Transactional
    private void insertRoles() {
        for (String role : roles) {
            String[] str = role.split(":");
            String name = str[0];
            String type = str[1];
            String pattern = str[2];
            String[] methods = str[3].split("\\|");
            Role oldRole = roleRepository.findOneByName(name);
            if (null == oldRole) {
                oldRole = new Role();
                oldRole.setName(name);
                oldRole.setType(type);
                oldRole.setRolePaths(new ArrayList<>());
                for (String m : methods) {
                    String rolePathName = name.toLowerCase() + "_" + m.toLowerCase();
                    RolePath rolePath = rolePathRepository.findOneByName(rolePathName);
                    if (null == rolePath) {
                        rolePath = new RolePath();
                        rolePath.setName(rolePathName);
                        rolePath.setMethod(m.toUpperCase());
                        rolePath.setPattern(pattern);
                        rolePath.setRole(oldRole);
                        rolePathRepository.save(rolePath);
                        oldRole.getRolePaths().add(rolePath);
                    }
                }
            }

            roleRepository.save(oldRole);
        }
    }

    @Transactional
    private void insertClients(String password) {
        for (String c : clients) {
            String[] s = c.split(":");
            String clientName = s[0];
            String[] clientRoles = s[1].split("\\s");
            Client oldClient = clientRepository.findOneByClientId(clientName);
            if (null == oldClient) {
                oldClient = new Client();
                oldClient.setClientId(clientName);
                oldClient.setAccessTokenValiditySeconds(28800);
                oldClient.setRefreshTokenValiditySeconds(7257600);
                oldClient.setGrantTypes("password refresh_token authorization_code");
                oldClient.setClientSecret(password);
                oldClient.setApproved(true);
                oldClient.setRedirectUris("");
                oldClient.setScopes("read write");
                List<Role> rls = roleRepository.findByNameIn(clientRoles);

                if (rls.size() > 0) {
                    oldClient.getAuthorities().addAll(rls);
                }
            }
            clientRepository.save(oldClient);
        }
    }

    @Transactional
    private void insertUser(String password) {
        for (String userNames : users) {
            String[] str = userNames.split(":");
            String username = str[0];
            String[] roleNames = str[1].split("\\s");

            User oldUser = userRepository.findOneByUsername(username);
            if (null == oldUser) {
                oldUser = new User();
                oldUser.setUsername(username);
                oldUser.setPassword(password);
                List<Role> r = roleRepository.findByNameIn(roleNames);
                oldUser.setRoles(r);
            }

            User obj = userRepository.save(oldUser);

            Profile profile = new Profile();
            profile.setFullname(str[2]);
            profile.setUser(obj);
            profileRepository.save(profile);
        }
    }

    @Transactional
    private void insertProject() {
        for (String projectNames : project) {
            String[] str = projectNames.split(":");
            String projectName = str[0];

            Project oldProject = projectRepository.findOneByProjectname(projectName);
            if (null == oldProject) {
                oldProject = new Project();
                oldProject.setProjectName(projectName);
                oldProject.setCategory(str[1]);
                oldProject.setProfilePeneliti("Mahasiswa");
                oldProject.setInstitution(str[2]);
                oldProject.setDuration(Integer.parseInt(str[3]));
                oldProject.setProjectEnd(LocalDate.now().plusDays(oldProject.getDuration()));
                oldProject.setProjectSummary(str[4]);
                oldProject.setBudget(str[5]);
                oldProject.setBudgetDetails(str[4]);
                oldProject.setProjectBackground(str[4]);
                oldProject.setProjectUrgency(str[4]);
                oldProject.setProjectGoal(str[4]);
                oldProject.setResearchMethods(str[4]);
                oldProject.setStatus(true);
                User user = userRepository.getByID(2L);
                oldProject.setUser(user);
            }

            projectRepository.save(oldProject);
        }
    }

}
