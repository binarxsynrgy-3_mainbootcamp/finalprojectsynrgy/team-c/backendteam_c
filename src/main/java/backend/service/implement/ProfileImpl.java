package backend.service.implement;

import backend.config.UploadFileResponse;
import backend.controller.main.forgetpassword.TemplateReqRes;
import backend.entity.Profile;
import backend.entity.User;
import backend.model.UpdatePasswordModel;
import backend.repository.ProfileRepository;
import backend.repository.UserRepository;
import backend.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Transactional
public class ProfileImpl implements ProfileService {

    private final ProfileRepository profileRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ProfileImpl(ProfileRepository profileRepository,
                       UserRepository userRepository,
                       PasswordEncoder passwordEncoder) {
        this.profileRepository = profileRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Map viewDetail(Long userId) {
        Map<String, Object> map = new HashMap<>();
        try {
//            User user = userRepository.getByID(userId);
            Profile profile = profileRepository.getByUserId(userId);
            map.put("data", profile);
            map.put("status", 200);
            map.put("message", "Get profile success");
            return map;
        }catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get profile");
            return map;
        }
    }

    @Override
    public Map update(Profile profile) {
        Map map = new HashMap();
        try {
            Profile obj = profileRepository.getById(profile.getId());
            if(obj == null) {
                map.put("StatusCode", 404);
                map.put("Status Messaga", "Data id belum ditemukan ");
                return map;
            }
            obj.setAddress(profile.getAddress());
            obj.setDob(profile.getDob());
            obj.setFullname(profile.getFullname());
            profileRepository.save(obj);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

    @Override
    public UploadFileResponse uploadProfile (MultipartFile file,
                                         Long id) {
        try {
            Profile obj = profileRepository.getById(id);
            obj.setPic_profile(file.getOriginalFilename());
            profileRepository.save(obj);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new UploadFileResponse(
                    file.getOriginalFilename(),
                    null,
                    file.getContentType(),
                    file.getSize(),
                    e.getMessage()
            );
        }

        String fileDownloadUri = ServletUriComponentsBuilder.
                fromCurrentContextPath().
                path("/user/showfile/").
                path(file.getOriginalFilename()).
                toUriString();

        return new UploadFileResponse(
                file.getOriginalFilename(),
                fileDownloadUri,
                file.getContentType(),
                file.getSize(),
                "false"
        );
    }

    //Service untuk ubah password di profile
    @Override
    public Map changePassword(UpdatePasswordModel updatePasswordModel) {
        Map<String, Object> map = new HashMap<>();
        try {
            User user = userRepository.findOneByUsername(updatePasswordModel.getEmail());
            if (!passwordEncoder.matches(updatePasswordModel.getOldPassword(), user.getPassword())) return TemplateReqRes.notFound("Password Lama salah");
            if (!Objects.equals(updatePasswordModel.getNewPassword1(), updatePasswordModel.getNewPassword2())) return TemplateReqRes.notFound("Ulangi Password");
            user.setPassword(passwordEncoder.encode(updatePasswordModel.getNewPassword1().replaceAll("\\s+", "")));
            User obj = userRepository.save(user);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

}
