package backend.service.implement;

import backend.config.Config;
import backend.controller.main.forgetpassword.TemplateReqRes;
import backend.entity.HistoryProject;
import backend.entity.Project;
import backend.entity.User;
import backend.repository.ProjectHistoryRepository;
import backend.repository.ProjectRepository;
import backend.repository.UserRepository;
import backend.service.HistoryProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class HistoryProjectImplement implements HistoryProjectService {

    private final UserRepository userRepository;

    private final ProjectRepository projectRepository;

    private final ProjectHistoryRepository projectHistoryRepository;

    @Value("${expired.token.password.minute:}")
    private int expiredToken;

    Config config = new Config();

    @Autowired
    public HistoryProjectImplement(UserRepository userRepository,
                                   ProjectRepository projectRepository,
                                   ProjectHistoryRepository projectHistoryRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.projectHistoryRepository = projectHistoryRepository;
    }

    // donatur
    @Override
    public Map insert(HistoryProject historyProject, long projectId) {
        Map<String, Object> map = new HashMap<>();
        try {
            User user = userRepository.getByID(historyProject.getUser().getId());
            Project project = projectRepository.getByID(projectId);
            historyProject.setUser(user);
            historyProject.setProject(project);
            historyProject.setStatus(true);
            project.setDonateAll((project.getDonateAll() + historyProject.getTotalDonate()));
            projectRepository.save(project);
            projectHistoryRepository.save(historyProject);
            map.put("data", "Thanks for your donation");
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map verifDonate(HistoryProject historyProject) {
        Map map = new HashMap();
        try {
            if (historyProject.getOtp() == null) return TemplateReqRes.notFound("Token " + config.isRequired);
            HistoryProject obj = projectHistoryRepository.findOneByOTP(historyProject.getOtp());
            if (obj == null) return TemplateReqRes.notFound("otp" + config.notFound);
            obj.setStatus(true);
            obj.setOtp(null);
            obj.setOtpExpiredDate(null);
            projectHistoryRepository.save(obj);
            map.put("data", "Verify donate success");
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }

    }

    @Override
    public Map viewHistory(Long userId) {
        List<HistoryProject> list;
        Map<String, Object> map = new HashMap<>();
        try {
            list = projectHistoryRepository.getByUserId(userId);
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get history by user success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error Get history by user");
            return map;
        }
    }

    // admin super user
    @Override
    public Map getAll() {
        List<HistoryProject> list = new ArrayList<HistoryProject>();
        Map map = new HashMap();
        try {
            list = projectHistoryRepository.getListUnproved();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all project");
            return map;
        }
    }

    @Override
    public Map getOne(Long projectId) {
        Map<String, Object> map = new HashMap<>();
        try {
            int total = projectHistoryRepository.getCountByTotalDonate(projectId);
            int potongantotal = 5 / 100 * total;
            HistoryProject obj = projectHistoryRepository.getByUnproved(projectId);
            map.put("data", obj);
            map.put("totaldonate", total);
            map.put("totaldonateafter5persen", potongantotal);
            map.put("status", 200);
            map.put("message", "Get project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get project");
            return map;
        }
    }

    @Override
    public Map acceptProject(Long id) {
        Map map = new HashMap();
        try {
            HistoryProject historyProject = projectHistoryRepository.getByID(id);
            if (historyProject == null) {
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }

            projectHistoryRepository.accept(id);

//            tambahan dari epan
            Project project = projectRepository.getByID(id);
            project.setStatusDonate(Boolean.TRUE);
            projectRepository.save(project);
//            akhir tambahan dari epan

            map.put("statuscode", 200);
            map.put("statusMessage", "project accepted");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

    @Override
    public Map declineProject(Long id) {
        Map map = new HashMap();
        try {
            HistoryProject historyProject = projectHistoryRepository.getByID(id);
            if (historyProject == null) {
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }

            projectHistoryRepository.decline(id);

            map.put("statuscode", 200);
            map.put("statusMessage", "project decline");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

    @Override
    public Map viewRequest(Long projectId) {
        Map<String, Object> map = new HashMap<>();
        List<HistoryProject> list = new ArrayList<HistoryProject>();
        try {
            list = projectHistoryRepository.getInfoDana(projectId);
            int totalAll = projectHistoryRepository.getCountByTotalDonate(projectId);
            int totals = (5 / 100 * totalAll);
            map.put("data", list);
            map.put("total", totalAll);
            map.put("total you can get", totals);
            map.put("status", 200);
            map.put("message", "Get dana success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error Get history by user");
            return map;
        }
    }

    // mahasiswa
    @Override
    public Map request(Long projectId) {
        Map map = new HashMap();
        try {
            HistoryProject historyProject = projectHistoryRepository.getByID(projectId);
            if (historyProject == null) {
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }

            projectHistoryRepository.request(projectId);

//            tambahan dari epan
            Project project = projectRepository.getByID(projectId);
            project.setStatusDonate(Boolean.FALSE);
            projectRepository.save(project);
//            akhir tambahan dari epan
            map.put("statuscode", 200);
            map.put("statusMessage", "request terkirim");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

}
