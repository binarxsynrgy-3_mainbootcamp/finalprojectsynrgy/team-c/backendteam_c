package backend.service.implement;

import backend.config.Config;
import backend.controller.main.forgetpassword.TemplateReqRes;
import backend.entity.HistoryProject;
import backend.entity.Project;
import backend.entity.User;
import backend.repository.ProjectHistoryRepository;
import backend.repository.ProjectRepository;
import backend.repository.UserRepository;
import backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Transactional
public class ProjectImplement implements ProjectService {

    private final UserRepository userRepository;

    private final ProjectRepository projectRepository;

    private final ProjectHistoryRepository projectHistoryRepository;

    Config config = new Config();

    @Autowired
    public ProjectImplement(UserRepository userRepository,
                            ProjectRepository projectRepository,
                            ProjectHistoryRepository projectHistoryRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.projectHistoryRepository = projectHistoryRepository;
    }

    @Override
    public Map insert(Project project) {
        Map<String, Object> map = new HashMap<>();
        try {
            User user = userRepository.getByID(project.getUser().getId());
            project.setUser(user);
            project.setProjectEnd(LocalDate.now().plusDays(project.getDuration()));
            Project obj = projectRepository.save(project);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map getByAdminId(Project project) {
        List<Project> list = new ArrayList<>();
        List<Project> temp1 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = projectRepository.getByAdminId(project.getUser().getId());
            for (int i = 0; i < list.size(); i++) {
                Project temp = list.get(i);
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                temp1.add(temp);
            }
            map.put("data", temp1);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map update(Project project) {
        Map<String, Object> map = new HashMap<>();
        try {
            Project obj = projectRepository.getByID(project.getId());
            obj.setProjectName(project.getProjectName());
            obj.setCategory(project.getCategory());
            obj.setProfilePeneliti(project.getProfilePeneliti());
            obj.setInstitution(project.getInstitution());
            obj.setDuration(project.getDuration());
            obj.setProjectEnd(LocalDate.now().plusDays(project.getDuration()));
            obj.setProjectSummary(project.getProjectSummary());
            obj.setProjectImg(project.getProjectImg());
            obj.setBudget(project.getBudget());
            obj.setBudgetDetails(project.getBudgetDetails());
            obj.setProjectBackground(project.getProjectBackground());
            obj.setProjectUrgency(project.getProjectUrgency());
            obj.setProjectGoal(project.getProjectGoal());
            obj.setResearchMethods(project.getResearchMethods());
            obj.setProjectDetails(project.getProjectDetails());
            obj.setCoverLetter(project.getCoverLetter());
            projectRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    public Map getOne(Long id) {
        Map<String, Object> map = new HashMap<>();
        List<HistoryProject> list;
        try {
            Project obj = projectRepository.getByID(id);
            obj.setDuration((int) DAYS.between(LocalDate.now(), obj.getProjectEnd()));
            list = projectHistoryRepository.findByProjectId(obj.getId());
            map.put("data", obj);
            map.put("dataDonate", list);
            map.put("status", 200);
            map.put("message", "Get project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get project");
            return map;
        }
    }

    @Override
    public Map getListPalingSedikit() {
        List<Project> list = new ArrayList<Project>();
        Map map = new HashMap();
        try {
            list =projectHistoryRepository.getProjectPalingSedikit();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all project");
            return map;
        }
    }

    // super admin
    @Override
    public Map getAll() {
        List<Project> list = new ArrayList<Project>();
        Map map = new HashMap();
        try {
            list = projectRepository.getList();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all project");
            return map;
        }
    }

    public Map getDetailRequest(Long id) {
        Map<String, Object> map = new HashMap<>();
        try {
            Project obj = projectRepository.getUnapproved(id);
            obj.setDuration((int) DAYS.between(LocalDate.now(), obj.getProjectEnd()));
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Get project success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get project");
            return map;
        }
    }

    @Override
    public Map acceptProject(Long id) {
        Map map = new HashMap();
        try {
            Project project = projectRepository.getByID(id);
            if (project == null) {
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }

            projectRepository.acceptStatus(id);

            map.put("statuscode", 200);
            map.put("statusMessage", "project accepted");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

    @Override
    public Map declineProject(Long id) {
        Map map = new HashMap();
        try {
            Project project = projectRepository.getByID(id);
            if (project == null) {
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }

            projectRepository.declineStatus(id);

            map.put("statuscode", 200);
            map.put("statusMessage", "project decline");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

    @Override
    public Map getListApproved() {
        List<Project> list1;
        List<Project> list2 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            list1 = projectRepository.getListApproved();
            if (list1.isEmpty()) return TemplateReqRes.notFound("No approved campaign");
            for (Project temp : list1) {
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                list2.add(temp);
            }
            map.put("data", list2);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map getProjectByProjectName(String projectName) {
        List<Project> list;
        List<Project> temp1 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = projectRepository.findByProjectName(projectName.toLowerCase());
            for (Project temp : list) {
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                temp1.add(temp);
            }
            map.put("data", temp1);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map getProjectByCategory(String category) {
        Page<Project> list;
        List<Project> temp1 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            Pageable pageable = PageRequest.of(0, 100);
            list = projectRepository.findByCategory(category, pageable);
            for (int i = 0; i < list.getNumberOfElements(); i++) {
                Project temp = list.getContent().get(i);
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                temp1.add(temp);
            }
            map.put("data", temp1);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map getProjectByDaysLeft() {
        List<Project> list;
        List<Project> temp1 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = projectRepository.getListApproved();
            for (Project temp : list) {
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                temp1.add(temp);
            }
            Collections.sort(temp1);
            map.put("data", temp1);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map getProjectByTerbaru() {
        Page<Project> list;
        List<Project> temp1 = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        try {
            Pageable pageable = PageRequest.of(0, 100, Sort.by("created_date").descending());
            list = projectRepository.findByTerbaru(pageable);
            for (int i = 0; i < list.getNumberOfElements(); i++) {
                Project temp = list.getContent().get(i);
                temp.setDuration((int) DAYS.between(LocalDate.now(), temp.getProjectEnd()));
                temp1.add(temp);
            }
            map.put("data", temp1);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }


}
