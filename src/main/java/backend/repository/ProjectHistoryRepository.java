package backend.repository;

import backend.entity.HistoryProject;
import backend.entity.Project;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectHistoryRepository extends PagingAndSortingRepository<HistoryProject, Long> {

    @Query("select h from HistoryProject h WHERE h.id = :id")
    HistoryProject getByID(@Param("id") Long id);

    @Query("select h from HistoryProject h WHERE h.id = :id and h.requestStatus=false")
    HistoryProject getByUnproved(@Param("id") Long id);

    @Query("SELECT  h from  HistoryProject  h WHERE h.project.status = false")
    public List<HistoryProject> getListUnproved();

    @Modifying
    @Query("update HistoryProject h set h.requestStatus = true where h.id= :id")
    void accept(@Param("id") Long id);

    @Modifying
    @Query("update HistoryProject h set h.requestStatus = false where h.id= :id")
    void decline(@Param("id") Long id);

    @Query("update HistoryProject h set h.status = :status where h.id= :id")
    public HistoryProject updateStatus(@Param("id") Long id, @Param("status") Boolean status);

    @Query("SELECT  h from  HistoryProject  h WHERE h.project.status = TRUE ")
    public List<HistoryProject> getList();

    @Query("FROM HistoryProject h WHERE h.otp = ?1")
    HistoryProject findOneByOTP(String otp);

    @Query(" FROM HistoryProject h WHERE h.project.id = :project_id")
    List<HistoryProject> findByProjectId(@Param("project_id") Long project_id);

    @Query("select h from HistoryProject h WHERE h.user.id = :id")
    List<HistoryProject> getByUserId(@Param("id") Long id);

    @Query("select sum(h.totalDonate) as total_donate from HistoryProject  h  WHERE  h.project.id =:id group by h.project.id ")
    int getCountByTotalDonate(@Param("id") Long id);

    @Query("select h as total_donate from HistoryProject  h  WHERE h.requestStatus = false and h.project.id =:id")
    List<HistoryProject> getInfoDana(@Param("id") Long id);

    @Modifying
    @Query("update HistoryProject h set h.requestStatus = true where h.id= :id")
    void request(@Param("id") Long id);

//    @Query(value = "SELECT h, sum(h.totalDonate) as total_donate FROM HistoryProject AS h group BY h.project.id  ORDER BY SUM (h.totalDonate) DESC", nativeQuery = true)
//    List<Project> getProjectPalingSedikit();

    @Query("SELECT h, sum(h.totalDonate) as total_donate FROM HistoryProject AS h group BY h.project.id  ORDER BY SUM (h.totalDonate) DESC")
    List<Project> getProjectPalingSedikit();


}
