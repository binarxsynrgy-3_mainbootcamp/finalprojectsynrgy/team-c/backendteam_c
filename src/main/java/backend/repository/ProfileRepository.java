package backend.repository;

import backend.entity.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends PagingAndSortingRepository<Profile, Long> {
    @Query("SELECT e FROM Profile e WHERE e.id = :id")
    Profile getById(@Param("id") Long id);


    @Query("SELECT e FROM Profile e WHERE e.user.id = :id")
    Profile getByUserId(@Param("id") Long id);


}
