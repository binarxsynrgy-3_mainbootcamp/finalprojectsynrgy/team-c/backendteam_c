package backend.repository;

import backend.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    @Query("SELECT p FROM Project p WHERE p.id = :id")
    Project getByID(@Param("id") Long id);

    @Query("SELECT p FROM Project p WHERE p.id = :id and p.status is null")
    Project getUnapproved(@Param("id") Long id);

    @Modifying
    @Query("SELECT p FROM Project p WHERE p.user.id = :user_id")
    List<Project> getByAdminId(@Param("user_id") Long user_id);

    @Modifying
    @Query("update Project p set p.status = false where  p.id= :id")
    void declineStatus(@Param("id") Long id);

    @Modifying
    @Query("update Project p set p.status = true where  p.id= :id")
    void acceptStatus(@Param("id") Long id);


    @Query("update Project p set p.status = :status where p.id= :id")
    public Project updateStatus(@Param("id") Long id, @Param("status") Boolean status);

    @Query("SELECT p from Project p")
    public List<Project> getList();

    @Query("FROM Project p WHERE LOWER(p.projectName) = LOWER(?1)")
    Project findOneByProjectname(String projectName);

    @Query("FROM Project p WHERE p.status = TRUE")
    List<Project> getListApproved();

    @Query("FROM Project p WHERE LOWER(p.projectName) LIKE %:projectName% AND p.status = TRUE")
    List<Project> findByProjectName(@Param("projectName") String projectName);

    @Query("FROM Project p WHERE p.status = TRUE")
    Page<Project> findByCategory(String category, Pageable pageable);

    @Query("FROM Project p WHERE p.status = TRUE")
    Page<Project> findByTerbaru(Pageable pageable);

}
